package com.gh.springsecuritydemo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

/**
 * @Author Eric
 * @Date 2021/7/4 23:09
 * @Version 1.0
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    @Qualifier("userDetailsService")
    private UserDetailsService userDetailsService;

    //注入数据源
    @Autowired
    private DataSource dataSource;

    @Bean
    public PersistentTokenRepository persistentTokenRepository(){
        JdbcTokenRepositoryImpl jdbcTokenRepository=new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);//配置数据源
        jdbcTokenRepository.setCreateTableOnStartup(true);//自动创建表
        return jdbcTokenRepository;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(userDetailsService).passwordEncoder(password());
    }

    @Bean
    PasswordEncoder password(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //退出配置
        http.logout().logoutUrl("/logout").logoutSuccessUrl("/admin/hello").permitAll();

        //配置自定义访问权限页面
        http.exceptionHandling().accessDeniedPage("/unauth.html");

        http.formLogin()   //自定义自己编写的登录页面
                .loginPage("/login.html")
                .loginProcessingUrl("/user/login") //登录访问路径
                .defaultSuccessUrl("/success.html").permitAll()  //登录成功跳转的路径
                .and().authorizeRequests()
                .antMatchers("/","/admin/hello","/user/login").permitAll() //设置哪些路径可以直接访问,不需要认证
//                //设置当前用户只有具备admins权限才可以访问
//                .antMatchers("/admin/index").hasAuthority("admins")
//                .antMatchers("/admin/index").hasAnyAuthority("admins","role")
                //hasAnyRole
                .antMatchers("/admin/index").hasAnyRole("sale,admin")

                .anyRequest().authenticated() //所有请求都可以访问
                .and().rememberMe().tokenRepository(persistentTokenRepository())//设置自动登录
                .tokenValiditySeconds(100)  //设置有效时间,单位秒
                .userDetailsService(userDetailsService);
                //.and().csrf().disable();  //关闭csrf防护
    }
}
