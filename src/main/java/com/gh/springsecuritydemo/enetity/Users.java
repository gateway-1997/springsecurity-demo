package com.gh.springsecuritydemo.enetity;

import lombok.Data;

/**
 * @Author Eric
 * @Date 2021/7/5 20:12
 * @Version 1.0
 */
@Data
public class Users {

    private Integer id;
    private String username;
    private String password;
}
