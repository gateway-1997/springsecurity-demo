package com.gh.springsecuritydemo.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Eric
 * @Date 2021/7/4 19:15
 * @Version 1.0
 */
@RestController
@RequestMapping("admin")
public class TestController {

    @GetMapping("hello")
    public String test1(){
        return "hello security";
    }
    @GetMapping("index")
    public String index(){
        return "success.html";
    }
    @GetMapping("update")
    //@Secured({"ROLE_sale","ROLE_manager"})
    //@PreAuthorize("hasAnyAuthority('sale')")
    @PostAuthorize("hasAnyAuthority('sales')")
    public String update(){
        System.out.println(111);
        return "hello update";
    }
}
