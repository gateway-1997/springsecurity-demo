package com.gh.springsecuritydemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gh.springsecuritydemo.enetity.Users;

public interface UsersMapper extends BaseMapper<Users> {
}
